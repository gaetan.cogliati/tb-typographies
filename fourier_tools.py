from constants import *
import numpy as np
import math
import cmath


def coefficients_fft(path, nb_coeff=NB_COEFF):
    """
    coefficient_fft computes the coefficient for every frequency k ∈ {-nb_coeff/2, ... , nb_coeff/2} (integer values),
    using the FFT.

    :param path: the outline path
    :param nb_coeff: the number of coefficients to compute
    :returns an array of coefficients, the spectrum
    """
    coords = np.array(path)
    x, y = coords[:, 0], coords[:, 1]
    complex_coords = x + 1j * y
    spectrum = np.fft.fft(complex_coords, norm='forward')
    length = len(spectrum)
    filtered = np.r_[spectrum[int(length - nb_coeff / 2):], spectrum[:int(nb_coeff / 2 + 1)]]
    return filtered.tolist()


def normalized_spectrum_fft(points, nb_coff=NB_COEFF):
    """
    Computes the normalized (amplitudes between 0 and 1) spectrum of the given outline's points using the FFT.
    The frequency 0 is omitted.

    :param points: the points along the outline
    :param nb_coeff: the nb of coefficients to compute for the spectrum
    :returns the spectrum, an array of coefficients
    """
    coefficients = coefficients_fft(points, nb_coff)
    # remove constant coefficient (k=0)
    del coefficients[int(nb_coff / 2)]
    # normalize coefficients
    max_amplitude = max(abs(cf) for cf in coefficients)
    return [cf / max_amplitude for cf in coefficients]


def coefficient_iteration(j, point, k, length):
    """
    coefficient_iteration computes one iteration, one part, of the sum function used to calculate a frequency coefficient.

    :param j: the point index (from the outline)
    :param point: the outline point
    :param k: the coefficient index
    :param length: the number of points on the outline
    :returns one iteration value part of the DFT sum
    """
    point_value = point[0] + 1j * point[1]
    return point_value * cmath.exp(-1j * j * k * 2 * math.pi / length)


def coefficients_dft(path, nb_coeff=NB_COEFF):
    """
    coefficient_dft computes the coefficient for every frequency k ∈ {-nb_coeff/2,.., nb_coeff/2} (integer values)

    :param path: the outline path
    :param nb_coeff: the number of coefficients to compute
    :returns an array of coefficients, the spectrum
    """
    res = []
    for k in range(int(-nb_coeff / 2), int(nb_coeff / 2 + 1)):
        sum = 0
        for j in range(len(path)):
            sum += coefficient_iteration(j + 1, path[j], k, len(path))
        res.append(sum / len(path))

    return res


def compute_coordinate(t, spectrum):
    """
    Compute the resulting point on the outline from a given spectrum for a specific time t. t ∈ [0,2π[

    :param t: time
    :param spectrum: the spectrum used to compute the point
    :returns a point on the outline defined by the spectrum
    """
    nb_coeff = len(spectrum)
    values = [spectrum[int(i + nb_coeff / 2)] * cmath.exp(1j * i * t) for i in
              range(int(-nb_coeff / 2), int(nb_coeff / 2 - 1))]
    return np.sum(np.array(values))


def outline_points(spectrum, nb_points=NB_POINTS):
    """
    Compute the outline points from the given Fourier spectrum (IDFT).

    :param spectrum: the spectrum evaluate
    :param nb_points: number of points on the outline
    :returns an array of points along on the outline defined by spectrum
    """

    frequencies = np.linspace(0, 2 * np.pi, nb_points)
    return np.array([compute_coordinate(t, spectrum) for t in frequencies])


def normalized_spectrum_dft(points, nb_coff=NB_COEFF):
    """
    Computes the normalized (amplitudes between 0 and 1) spectrum of the given outline's points. The frequency 0 is
    omitted.

    :param points: the points along the outline
    :param nb_coeff: the nb of coefficients to compute for the spectrum
    :returns the spectrum, an array of coefficients
    """
    coefficients = coefficients_dft(points, nb_coff)
    # remove constant coefficient (k=0)
    del coefficients[int(nb_coff / 2)]
    # normalize coefficients
    max_amplitude = max(abs(cf) for cf in coefficients)
    return [cf / max_amplitude for cf in coefficients]
