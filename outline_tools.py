from fontTools.pens.basePen import BasePen
from constants import *
import math
import bezier
import numpy as np


class GlyphPen(BasePen):
    """
    GlyphPen is used to 'draw' a glyph. It parses the segments and curves from the glyph, storing them in an array.
    """

    def __init__(self, glyphSet):
        super().__init__(glyphSet)
        self.points = []
        self.current_point = None
        self.nb_outline = 0
        self.path_segments = []
        self.current_path = []
        self.starting_points = []

    def _moveTo(self, p0):
        if len(self.current_path) != 0:
            self.path_segments.append(self.current_path)
        self.current_path = []
        self.nb_outline += 1
        self.starting_points.append(p0)

        self.current_point = p0

    def _lineTo(self, p1):
        self.current_path.append(('line', [self.current_point, p1]))
        self.current_point = p1

    def _curveToOne(self, p1, p2, p3):
        bz_control_points = [self.current_point, p1, p2, p3]
        self.current_path.append(('curve', bz_control_points))
        self.current_point = p3

    def _closePath(self):
        self.current_path.append(('line', [self.current_point, self.starting_points[self.nb_outline - 1]]))

    def get_segments(self):
        if len(self.current_path) != 0:
            self.path_segments.append(self.current_path)
        return self.path_segments




def get_segments(font, ch):
    """
    get_segment returns the segments composing the outline(s) of the glyph of the given character in the given font.

    :param font: the font to take the glyph from
    :param ch: the character
    """
    glyph_name = font.getBestCmap().get(ord(ch))
    glyph = font.getGlyphSet()[glyph_name]
    p = GlyphPen(font.getGlyphSet())
    glyph.draw(p)
    return p.get_segments()



def straight_line_length(start, end):
    """
    straight_line_length computes the length of the segment defined by the 'start' and 'end' points.
    :param start: the starting point of the segment
    :param end: the ending point of the segment
    """
    x = end[0] - start[0]
    y = end[1] - start[1]
    return math.sqrt(x * x + y * y)



def evaluate_line_point(start, end, t):
    """
    evaluate_line_point return a point along the line defined by the 'start' and 'end point' at a distance
    equal to the length of the line times t, t being between 0 and 1.

    :param start: the starting point of the segment
    :param end: the ending point of the segment
    :returns a point placed at a distance of length*t between start and end.
    """
    x = end[0] - start[0]
    y = end[1] - start[1]
    # straight vertical line
    if x == 0:
        return (start[0], t * y + start[1])

    # Non-vertical line
    slope = y / x
    y_intercept = start[1] - slope * start[0]
    progression = t * x + start[0]

    return (progression, progression * slope + y_intercept)



def counterclockwise(p1, p2, p3):
    """
    counterclockwise determines whether the points are given in a clockwise or counterclockwise order.

    :param p1: first point
    :param p2: middle point
    :param p3: end point
    :returns -1 if clockwise, 1 if counterclockwise, 0 if all are aligned
    """
    # negative if clockwise, twice the area of the triangle p1 - p2 - p3
    a = (p2[0] - p1[0]) * (p3[1] - p1[1]) - (p3[0] - p1[0]) * (p2[1] - p1[1])
    if a < 0:
        return -1
    if a > 0:
        return 1

    return 0



def top_left(p1, top_left):
    """
    top_left returns True if the given point p1 if more to the left (and/or higher) than the leftmost given point 'top_left'
    return False otherwise.

    :param p1: the point to compare with top_left
    :param top_left: the highest leftmost point
    """
    if p1[0] < top_left[0]:
        return True
    if p1[0] == top_left[0] and p1[1] >= top_left[1]:
        return True
    return False



def path_counterclockwise(segments):
    """
    path_counterclockwise determines whether the segments (and therefore the overall shape/outline) are given in a clockwise or
    counterclockwise order.
    :param segments: the outline's segments
    :returns -1 if clockwise, 1 if counterclockwise, 0 if all are aligned
    """
    length = len(segments)
    if length < 2:
        return None

    # set extreme to first point on the outline
    extreme = segments[0][1][0]
    points = [extreme]
    choice_index = 0
    starting_point = extreme
    for i in range(0, length):
        segment = segments[i]
        if segment[0] == 'line':
            point = segment[1][1]
            if point == starting_point:
                break
            points.append(point)
            if top_left(point, extreme):
                extreme = point
                choice_index = i + 1
        elif segment[0] == 'curve':
            point = segment[1][-1]
            if point == starting_point:
                break
            points.append(point)
            if top_left(point, extreme):
                extreme = point
                choice_index = i + 1
    nb_points = len(points)
    return counterclockwise(points[(choice_index + nb_points - 1) % nb_points], extreme,
                            points[(choice_index + 1) % nb_points])



def evaluate_points(segments, nb_points=NB_POINTS):
    """
    evaluate_points return a list of evenly-spaced points along the segments given as input.
    The points are always taken clockwise.

    :param segments: the evaluated outline's segments
    :param nb_points: the total number of points to evaluate
    :returns an array of evenly spaced points on the segments
    """
    total_length = 0
    segment_lengths = []
    for segment in segments:
        if segment[0] == 'line':
            length = straight_line_length(segment[1][0], segment[1][1])
        elif segment[0] == 'curve':

            length = bezier.Curve(np.array(segment[1], dtype=float).T, degree=3).length
        segment_lengths.append(length)
        total_length += length

    intervals = np.linspace(0, total_length, nb_points, endpoint=False)
    points = []
    current_length = 0
    segment_index = 0
    point_index = 0
    while point_index < nb_points and segment_index < len(segment_lengths):
        # If next point still in current segment
        if intervals[point_index] <= current_length + segment_lengths[segment_index]:
            # evaluate point on segment
            segment_progress = (intervals[point_index] - current_length) / segment_lengths[segment_index]
            segment = segments[segment_index]

            if segment[0] == 'line':
                start, end = segment[1]
                point = evaluate_line_point(start, end, segment_progress)
            elif segment[0] == 'curve':
                curve = bezier.Curve(np.array(segment[1], dtype=float).T, degree=3)
                point = curve.evaluate(segment_progress)
                point = [point[0][0], point[1][0]]
            points.append(point)
            point_index += 1
        else:
            # go to next segment
            current_length += segment_lengths[segment_index]
            segment_index += 1

    if path_counterclockwise(segments) == 1:
        # always use the points clockwise
        points = list(reversed(points))
    return points



def line(bitmap, x0, y0, x1, y1, outline_color):
    """
    Bresenham's line algorithm. Draws a straight line (as close as possible) in a bitmap

    :param x0: value of the first point on the x-axis
    :param y0: value of the first point on the y-axis
    :param x1: values of the second point on the x-axis
    :param y1: values of the second point on the y-axis
    """
    x0 = int(round(x0))
    x1 = int(round(x1))
    y0 = int(round(y0))
    y1 = int(round(y1))
    dx = abs(x1 - x0)
    dy = abs(y1 - y0)
    x, y = x0, y0
    sx = -1 if x0 > x1 else 1
    sy = -1 if y0 > y1 else 1
    if dx > dy:
        err = dx / 2.0
        while x != x1:
            bitmap[y, x] = outline_color
            err -= dy
            if err < 0:
                y += sy
                err += dx
            x += sx
    else:
        err = dy / 2.0
        while y != y1:
            bitmap[y, x] = outline_color
            err -= dx
            if err < 0:
                x += sx
                err += dy
            y += sy
    bitmap[y, x] = outline_color


def draw_outline_lines(bitmap, outline, outline_color):
    """

    :param bitmap:
    :param outline:
    :param outline_color:
    :return:
    """
    # Draw the outline (straight line between points)
    for i in range(len(outline) - 1):
        x0, y0 = outline[i]
        x1, y1 = outline[i + 1]
        line(bitmap, x0, y0, x1, y1, outline_color)

    # Close the outline
    if len(outline) > 1:
        x0, y0 = outline[-1]
        x1, y1 = outline[0]
        line(bitmap, x0, y0, x1, y1, outline_color)
