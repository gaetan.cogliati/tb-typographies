# TB-Typographies

This program was created in the context of a Bachelor project at the [HEIA-FR](https://www.heia-fr.ch/).
Its purpose is to extract the Fourier spectra from character glyphs in different fonts.

## Install libraries

Install the necessary libraries beforehand

```shell
pip install -r requirements.txt
```

### Prepare your fonts and run the script

The program expects a zip file containing font files (as of now only .otf, .ttf and .ttc are supported).
You can arrange your fonts by category by putting them in separate folders before compressing them.

It will check what folders exist in the file and create a category for each of them based on their name.

![example selection](./img/example_de_selection.png)

Compress the data you can then run the script. There are 3 parameter:
- -f or --file : to specify the zip file containing
your fonts. 
- -m or --method : to choose the algorithm 'fft' for Fast Fourier Transform (default) or 'dft' for Discrete Fourier Transform 
- -d or --dirpath : to indicate the directory where you want the output file to be created.

```shell
python3 font_spectrum.py -f ./MySelection.zip   
```

The script will create a csv file 'FontSpectra.csv' with the spectra data with a column for each category.

![example selection](./img/csv_example.png)




