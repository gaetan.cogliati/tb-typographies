import argparse
import os
import matplotlib.pyplot as plt
import pandas as pd


def create_graph(data, output_dir):

    error_columns = data.columns[2:]
    os.makedirs(output_dir, exist_ok=True)

    for index, row in data.iterrows():
        plt.figure(figsize=(10, 6))

        plt.plot(error_columns, row[error_columns], marker='o')
        plt.xlabel('Number of points')
        plt.ylabel('Error')
        plt.title(f'Error Measures for {row["Character"]} - {row["Font"]}')
        plt.xticks(rotation=45)

        filename = f'{row["Character"]}_{row["Font"]}.png'
        output_path = os.path.join(output_dir, filename)
        plt.savefig(output_path, format='png')
        plt.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='Font spectrum',
        description='Character outline decomposition using Fourier analysis')

    parser.add_argument('-f', '--file')
    parser.add_argument('-d', '--outputdir')
    args = parser.parse_args()

    data_path = args.file
    output = args.outputdir
    data = pd.read_csv(data_path)
    create_graph(data, output_dir=output)
