import matplotlib.pyplot as plt
import numpy as np


def plot_outline(x, y, title):
    fig, ax = plt.subplots()
    ax.plot(x, y, 'bo', markersize=0.3)
    # ax.plot(x_coords, y_coords, 'b-', linewidth=1)
    ax.set_aspect('equal')
    plt.title(title)
    plt.xlabel('X Coordinates')
    plt.ylabel('Y Coordinates')
    plt.show()


def plot_normalized_coeffs(spectrum1, spectrum2, nb_coeff):
    spectrum1 = np.insert(spectrum1, int(nb_coeff / 2), 0)
    spectrum2 = np.insert(spectrum2, int(nb_coeff / 2), 0)
    x_coords = [i - nb_coeff / 2 for i in range(int(nb_coeff / 2) * 2 + 1)]
    y_coords = np.abs(spectrum1)

    x_coords2 = [i - nb_coeff / 2 for i in range(int(nb_coeff / 2) * 2 + 1)]
    y_coords2 = np.abs(spectrum2)

    fig, ax = plt.subplots(2)
    ax[0].stem(x_coords, y_coords)
    ax[1].stem(x_coords2, y_coords2)
    plt.xticks(range(int(-nb_coeff / 2), int(nb_coeff / 2 + 1), 5))
    # plt.title('Amplitude of frequencies (manual DFT)')
    plt.xlabel('frequency K')
    ax[0].set_ylabel('Amplitude')
    ax[1].set_ylabel('Amplitude')
    plt.show()


def plot_freq_amplitudes(x, y, title):
    fig, ax = plt.subplots()
    plt.stem(x, y)
    plt.xticks(range(int(-len(x) / 2 - 1), int(len(x) / 2), 5))
    plt.title(title)
    plt.xlabel('frequency K')
    plt.ylabel('Amplitude')
    plt.show()


def plot_control_points(points):
    x_coords_offcurve = []
    y_coords_offcurve = []
    x_coords_oncurve = []
    y_coords_oncurve = []
    for point in points:
        cmd = point[0]
        if cmd == 'moveTo':
            x_coords_oncurve.append(point[1][0])
            y_coords_oncurve.append(point[1][1])
        if cmd == 'lineTo':
            x_coords_oncurve.append(point[1][0])
            y_coords_oncurve.append(point[1][1])
        elif cmd == 'curveTo':
            x_coords_oncurve.append(point[3][0])
            y_coords_oncurve.append(point[3][1])
            x_coords_offcurve.append(point[1][0])
            y_coords_offcurve.append(point[1][1])
            x_coords_offcurve.append(point[2][0])
            y_coords_offcurve.append(point[2][1])

    # Show on-curve points in blue and off-curve points in red
    fig, ax = plt.subplots()
    ax.plot(x_coords_oncurve, y_coords_oncurve, 'bo', markersize=2)
    ax.plot(x_coords_offcurve, y_coords_offcurve, 'ro', markersize=1.5)
    ax.set_aspect('equal')
    plt.title('Control Points')
    plt.xlabel('X Coordinates')
    plt.ylabel('Y Coordinates')
    plt.show()
