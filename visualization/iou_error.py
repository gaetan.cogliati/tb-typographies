from os import listdir

import pandas as pd
import plotnine as p9


def create_heatmap(file, name):
    measure = pd.read_csv(file)
    measure['resolution'] = measure['resolution'].astype('category')
    measure['Nb frequencies'] = measure['Nb frequencies'].astype('category')

    colors = ['#1a9641', '#a6d96a', '#ffffbf', '#33FFF6', '#334CFF', '#9F33FF', '#FF0000']

    plot = (p9.ggplot(data=measure, mapping=p9.aes(x='resolution', y='Nb frequencies', fill='IoU'))
            + p9.geom_tile(color='black')
            + p9.scale_fill_gradientn(colors=colors, limits=[0.4, 1],
                                      values=[0.0, 0.7, 0.8, 0.9, 0.95, 0.99, 1.0])
            + p9.guides(color=p9.guide_legend(
                nrow=10,
                title='IoU'
            ))
            + p9.ggtitle(f'{name} HEATMAP'))
    plot.save(f'{name}_HEATMAP.png')


directory_path = '/Users/isc/TB/TestData/IoUData/cyrillic/'

for file in listdir(directory_path):
    create_heatmap(directory_path + file, file[:-4])
