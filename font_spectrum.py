import argparse
import os
import sys

from fourier_tools import *
from outline_tools import *
from fontTools.ttLib import TTFont, TTCollection, TTLibError
from visualization.plots import *
import numpy as np
import csv
import traceback
import zipfile
from zipfile import BadZipFile
import io


def extract_fonts_from_zip(file_path):
    """
    Extract all font files (.ttc, .otf, .ttc) form the given zip file.

    :param file_path: the zip file containing the fonts
    :returns an array containing the font file bytes
    """
    with zipfile.ZipFile(file_path, 'r') as zip_f:
        font_list = []
        for file_name in zip_f.namelist():
            if '__MACOSX' in file_name:
                continue
            name = file_name.split('/')[-1]
            if name.endswith(('.otf', '.ttf', '.ttc')):
                if name not in [font[0] for font in font_list]:
                    font_list.append([name, zip_f.read(file_name)])
        return font_list


def get_font_categories(file_path):
    """
    Get the font categories based on the folder they were in, within the given zip file.

    :param file_path: the zip file containing the fonts
    :returns bi-dimensional array containing the categories and font belonging to them
    """
    font_categories = {}
    with zipfile.ZipFile(file_path, 'r') as zip_f:
        dirs = list(set([os.path.dirname(x) for x in zip_f.namelist() if '__MACOSX' not in x]))
        file_list = [f for f in zip_f.namelist() if '__MACOSX' not in f and not f.endswith('/')]
        for file in file_list:
            category = os.path.dirname(file)
            if category == '':
                continue
            file_name = os.path.basename(file)
            if category not in font_categories:
                font_categories[category] = []
            font_categories[category].append(file_name)

    return font_categories


def display_spectrum(points, nb_coeff=NB_COEFF):
    """
    display the original outline (points) and the resulting outline after computing the spectrum.

    :param points: the outline's points
    :param nb_coeff: the number of coefficients to compute
    """
    x_coords, y_coords = zip(*points)
    plot_outline(x_coords, y_coords, 'Glyph outline')

    # Using manual calculation
    spectrum_mano = coefficients_dft(points)

    x_coords = [i - NB_COEFF / 2 for i in range(int(NB_COEFF / 2) * 2 + 1)]
    y_coords = np.abs(spectrum_mano)

    plot_freq_amplitudes(x_coords, y_coords, 'Amplitude of frequencies (manual DFT)')

    resulting_path_mano = outline_points(spectrum_mano)
    x_coords = [real for real in np.real(resulting_path_mano)]
    y_coords = [imag for imag in np.imag(resulting_path_mano)]

    # Show resulting outline (regular DFT)
    plot_outline(x_coords, y_coords, 'Glyph using dft')

    # Using numpy FFT
    fft_spectrum = coefficients_fft(points)
    resulting_path = outline_points(fft_spectrum)

    x_coords = [real for real in np.real(resulting_path)]
    y_coords = [imag for imag in np.imag(resulting_path)]
    plot_outline(x_coords, y_coords, 'Glyph using fft')


def write_font_spectrum(font, categories, file_name, writer, method='dft'):
    """
    Evaluates the spectra of all desired characters in the given font, and writes the spectra in a file (using the
    give writer)

    :param font: the font to evaluate
    :param categories: the font categories and list of fonts belonging to them
    :param file_name: the fonts file name
    :param writer: the writer used to write to a file
    :param method: method used to compute the spectra, 'fft' or 'dft'
    """
    font_type = None
    if 'glyf' in font:
        font_type = "TrueType"
    elif 'CFF ' in font or 'CFF2' in font:
        font_type = "OpenType/CFF"
    else:
        return

    font_name = font['name'].getDebugName(4)

    parsed_points = None
    for key, value in font.getBestCmap().items():
        if chr(key) in DESIRED_CHARACTERS:
            glyph_outlines = get_segments(font, chr(key))
            if len(glyph_outlines) != 1:
                continue

            # Evaluate points of the first outline
            parsed_points = evaluate_points(glyph_outlines[0])
            # display_spectrum(parsed_points)
            spectrum_dft = normalized_spectrum_dft(parsed_points) if method == 'dft' else normalized_spectrum_fft(
                parsed_points)
            csv_row = [f'{np.real(coefficient)} + {np.imag(coefficient)}' for coefficient in spectrum_dft]
            # insert row info: font name, type, character, categories
            selected_categories = [file_name in category[1] for category in categories]
            csv_row = [font_name, font_type, chr(key)] + selected_categories + csv_row
            writer.writerow(csv_row)


def is_regular(font):
    """
    determines whether the font is of type 'Regular', and is neither Bold nor Italic.

    :param font: the font to evaluate
    :returns True if 'Regular', False otherwise
    """
    os2 = font['OS/2']
    name = font['name'].getDebugName(4)
    if 'Bold' in name or 'Light' in name:
        return False
    # check that font is 'regular' (bit 6) and is neither bold (bit 5) nor italic (bit 9) nor oblique
    return os2.fsSelection >> 6 & 1 == 1 and os2.fsSelection & 1 == 0 and os2.fsSelection >> 5 & 1 == 0


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        prog='Font spectrum',
        description='Character outline breakdown using Fourier analysis')

    parser.add_argument('-f', '--file')
    parser.add_argument('-m', '--method', choices=['fft', 'dft'], default='fft')
    parser.add_argument('-d', '--dirpath', default='')
    args = parser.parse_args()

    zip_path = args.file

    try:
        categories = get_font_categories(zip_path)
        fonts = extract_fonts_from_zip(zip_path)
    except FileNotFoundError:
        print(f"Error: The file '{args.file}' was not found.")
        sys.exit(1)
    except BadZipFile:
        print('The File given as input is not a zip file')
        sys.exit(1)

    if len(fonts) == 0:
        print('No Fonts found in the given zip file')
        sys.exit(1)

    if args.method not in ['dft', 'fft']:
        print("Invalid method, use 'fft' or 'dft'")
        sys.exit(1)

    # Compute spectrum of all characters in every given font using the 'manual' DFT
    with open(f'{args.dirpath}FontSpectra.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        header_row = list(categories.keys())
        header_row = ["Font", "Font Format", "Character"] + header_row
        header_row += [str(i) for i in range(int(-NB_COEFF / 2), int(NB_COEFF / 2 + 1)) if i != 0]
        writer.writerow(header_row)
        for font_file in fonts:
            try:
                # Handle .ttc files
                if font_file[0].endswith(('.ttc', '.TTC')):
                    try:
                        collection = TTCollection(io.BytesIO(font_file[1]))
                        for font in collection:
                            if is_regular(font):
                                write_font_spectrum(font, categories.items(), font_file[0], writer, args.method)
                                break
                    finally:
                        collection.close()
                else:

                    trueTypeFont = TTFont(io.BytesIO(font_file[1]))
                    try:
                        write_font_spectrum(trueTypeFont, categories.items(), font_file[0], writer, args.method)
                    finally:
                        trueTypeFont.close()
                print(f'{font_file[0]} spectra  done')
            except (TTLibError, IOError) as e:
                print(traceback.format_exc())
                print(f"Error handling font '{font_file[0]}', skipping")
            except Exception as e:
                print(f"An unexpected error occurred: {e}")
                print(traceback.format_exc())
