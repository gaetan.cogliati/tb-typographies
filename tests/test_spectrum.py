import csv
import unittest
import io
import numpy as np
from fontTools.ttLib import TTFont, TTLibError
import font_spectrum
from fourier_tools import normalized_spectrum_dft, normalized_spectrum_fft
from outline_tools import evaluate_points, get_segments


BASE_NB_POINTS = 10000
NB_POINTS_STEP = 50
NB_STEPS = 30
PRECISION = 0.005


def error_sum(glyph_segments):
    """
    Compute the sum of error on the amplitudes comparing with a spectrum computed with BASE_NB_POINTS

    :param glyph_segments: the glyph outline to compare
    :return: an array containing the error sum for each number of points used
    """
    base_points = evaluate_points(glyph_segments[0], nb_points=BASE_NB_POINTS)
    base_spectrum = normalized_spectrum_fft(base_points)
    results = []
    for i in range(1, NB_STEPS + 1):
        sum = 0
        nb_points = i * NB_POINTS_STEP
        points = evaluate_points(glyph_segments[0], nb_points=nb_points)
        spectrum = normalized_spectrum_fft(points)
        for j in range(len(spectrum)):
            sum += np.abs(np.abs(base_spectrum[j]) - abs(spectrum[j]))
        results.append(sum)

    return results


class TestSpectrum(unittest.TestCase):
    """
    This test is to ensure the spectrum is independent of the font file format.
    """
    def test_coeff_amplitude_otf_ttf(self):
        ttf_font_path = './TestFonts/BebasNeue/BebasNeue-Regular.ttf'
        otf_font_path = './TestFonts/BebasNeue/BebasNeue-Regular.otf'
        otf_font = TTFont(otf_font_path)
        ttf_font = TTFont(ttf_font_path)
        nb_coefficients = 20
        for char in font_spectrum.DESIRED_CHARACTERS:
            otf_segments = get_segments(otf_font, char)
            ttf_segments = get_segments(ttf_font, char)
            if len(otf_segments) > 1 or len(ttf_segments) > 1:
                self.assertEqual(len(otf_segments), len(ttf_segments))
                continue
            otf_points = evaluate_points(otf_segments[0], nb_points=BASE_NB_POINTS)
            # font_spectrum.display_spectrum(otf_points)
            ttf_points = evaluate_points(ttf_segments[0], nb_points=BASE_NB_POINTS)
            # font_spectrum.display_spectrum(ttf_points)
            otf_spectrum = normalized_spectrum_dft(otf_points, nb_coefficients)
            ttf_spectrum = normalized_spectrum_dft(ttf_points, nb_coefficients)
            #visualization.plots.plot_normalized_coeffs(otf_spectrum, ttf_spectrum, nb_coefficients)

            # The phase of each coeff (argument) could be different but the amplitudes (abs values) must match
            otf_amplitude = int(np.abs(otf_spectrum[int(nb_coefficients / 2)]) * 1000)
            ttf_amplitude = int(np.abs(ttf_spectrum[int(nb_coefficients / 2 - 1)]) * 1000)

            for i in range(len(otf_spectrum)):
                otf_amplitude = np.abs(otf_spectrum[i])
                ttf_amplitude = np.abs(ttf_spectrum[i])

                self.assertAlmostEqual(otf_amplitude, ttf_amplitude, 1,
                                       f"Coeff for frequency {i - nb_coefficients / 2}, char {char}")

    def test_nb_points(self):

        zip_path = './TestFonts/testFonts.zip'
        fonts = font_spectrum.extract_fonts_from_zip(zip_path)
        tested_characters = set("cEFHMWXYZ")
        with open('Spectrum_error_sum.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            header_row = [str(i * NB_POINTS_STEP) for i in range(1, NB_STEPS + 1)]
            header_row.insert(0, "Character")
            header_row.insert(0, "Font")
            writer.writerow(header_row)

            for font_file in fonts:
                font = None
                try:
                    font = TTFont(io.BytesIO(font_file[1]))
                except TTLibError as e:
                    print(f"Problem reading font{font_file[0]} skipping")
                    continue

                for key, value in font.getBestCmap().items():
                    if chr(key) in tested_characters:
                        glyph_segments = get_segments(font, chr(key))
                        if len(glyph_segments) > 1:
                            continue
                        res = error_sum(glyph_segments)
                        res.insert(0, chr(key))
                        res.insert(0, font_file[0])
                        writer.writerow(res)

                print(f"Error sum for characters of font {font_file[0]} done")


if __name__ == '__main__':
    unittest.main()
