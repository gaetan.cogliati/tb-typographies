import csv
import unittest
import font_spectrum
import io
from fourier_tools import *
from outline_tools import *
from fontTools.ttLib import TTFont
from visualization.plots import *

# Tested resolutions
RESOLUTIONS = [30, 50, 100, 200]

MARGIN = 1.2
NB_POINTS = 2000
NB_STEPS = 30
STEP_SIZE = 10
OUTLINE_COLOR = 0
OUTSIDE_FILL_COLOR = 122


def find_point_outside(bitmap):
    """
    Finds a point that is on the outside of the glyph within the given bitmap.

    :param bitmap: the bitmap to search.
    """
    width, height = bitmap.shape[1], bitmap.shape[0]

    for i in range(width):
        for j in range(height):
            if bitmap[j, i] != 255:
                continue
            found = True
            # check for intersection with outline in vertical direction
            for k in range(j + 1, height - 1):
                if bitmap[k, i] != 255:
                    found = False
                    break
            if found:
                return i, j





def flood_fill(bitmap, x, y, fill_value):
    """
    Flood fill algorithm. Fills all pixels in the same area with the given color.

    :param bitmap: the bitmap to use flood fill on
    :param x: the value of the starting point on x-axis
    :param y: the value of the starting point on y-axis
    :param fill_value: the color the fill with
    """
    width, height = bitmap.shape[1], bitmap.shape[0]
    if bitmap[y, x] != 255:
        return

    queue = [(x, y)]
    while queue:
        cx, cy = queue.pop(0)
        if bitmap[cy, cx] == 255:
            bitmap[cy, cx] = fill_value
            if cx > 0:
                if bitmap[cy, cx - 1] == 255:
                    queue.append((cx - 1, cy))
            if cx < width - 1:
                if bitmap[cy, cx + 1] == 255:
                    queue.append((cx + 1, cy))
            if cy > 0:
                if bitmap[cy - 1, cx] == 255:
                    queue.append((cx, cy - 1))
            if cy < height - 1:
                if bitmap[cy + 1, cx] == 255:
                    queue.append((cx, cy + 1))


def letter_frequencies_iou(font, character):
    """
    Draws both the original glyph and the outline resulting from the spectra (with varying number of coefficients) on a
    bitmap (varying the resolution as well) then compares the area of both based on the IoU approach
    (Intersection over Union).

    :param font: font to use
    :param character: the evaluated character
    :return: an array containing results IoU value, for each variation of resolution and number of coefficients
    """
    # Get the original outline, 'draw' it in the bitmap (rescaled)
    glyph_outlines = get_segments(font, character)
    if len(glyph_outlines) > 1:
        return []

    outline = evaluate_points(glyph_outlines[0], nb_points=NB_POINTS)
    # Get the outline's outermost points (bounding box)
    max_y = 0
    min_y = 0
    max_x = 0
    min_x = 0
    for point in outline:
        x = point[0]
        y = point[1]
        if x > max_x:
            max_x = x
        if x < min_x:
            min_x = x
        if y > max_y:
            max_y = y
        if y < min_y:
            min_y = y

    # offset the glyph outline points to make sure they only have positive values
    original_glyph_size = max(max_y - min_y, max_x - min_x)
    margin_value = int(original_glyph_size * MARGIN) - original_glyph_size
    y_offset = -min_y + margin_value / 4 if min_y < 0 else margin_value / 4
    x_offset = -min_x + margin_value / 4 if min_x < 0 else margin_value / 4
    outline = [(point[0] + x_offset, point[1] + y_offset) for point in outline]

    results = []

    for i in range(1, NB_STEPS):
        nb_coeff = i * STEP_SIZE
        spectrum = coefficients_dft(outline, nb_coeff=nb_coeff)

        for resolution in RESOLUTIONS:
            # scale the original outline and draw it
            ratio = resolution / original_glyph_size
            outline_scaled = [(point[0] * ratio, point[1] * ratio) for point in outline]
            size = int(resolution * MARGIN)
            bitmap = np.ones((size, size), dtype=np.uint8) * 255
            draw_outline_lines(bitmap, outline_scaled, OUTLINE_COLOR)

            # Find a point outside the letter
            outside = find_point_outside(bitmap)
            # Fill pixels outside using flood fill
            flood_fill(bitmap, outside[0], outside[1], OUTSIDE_FILL_COLOR)
            base_area = np.count_nonzero((bitmap == OUTLINE_COLOR) | (bitmap == 255))
            # imageio.imwrite('output.png', bitmap)

            # scale the spectrum, get the new outline, draw it and fill the part outside the letter
            spectrum_scaled = [coeff * ratio for coeff in spectrum]
            resulting_points = outline_points(spectrum_scaled, nb_points=NB_POINTS)
            x_coords = [real for real in np.real(resulting_points)]
            y_coords = [imag for imag in np.imag(resulting_points)]

            new_outline = [(x, y) for x, y in zip(x_coords, y_coords)]
            res_bitmap = np.ones((size, size), dtype=np.uint8) * 255
            draw_outline_lines(res_bitmap, new_outline, OUTLINE_COLOR)
            ext = find_point_outside(res_bitmap)
            flood_fill(res_bitmap, ext[0], ext[1], OUTSIDE_FILL_COLOR)


            overlap = 0
            # Compare the two, original and result bitmap
            for y in range(size):
                for x in range(size):
                    if res_bitmap[y, x] != OUTSIDE_FILL_COLOR and bitmap[y, x] != OUTSIDE_FILL_COLOR:
                        overlap += 1

            area = np.count_nonzero((res_bitmap == OUTLINE_COLOR) | (res_bitmap == 255))
            iou = overlap / (base_area + area - overlap)

            # insert row info: resolution, number of frequencies and IoU
            csv_row = [resolution, nb_coeff, iou]
            results.append(csv_row)

    return results


class TestNbFrequency(unittest.TestCase):

    def test_frequency_iou(self):
        tested_characters = set("cWXHy")
        zip_path = '/Users/isc/TB/Fonts/Test/test_fonts.zip'
        fonts = font_spectrum.extract_fonts_from_zip(zip_path)
        header_row = ["resolution", "Nb frequencies", "IoU"]
        for font_file in fonts:
            try:
                font = TTFont(io.BytesIO(font_file[1]))
                print(f"Starting Font {font_file[0]}")
                for char in tested_characters:
                    # Create a csv for every character
                    with open(f'{font_file[0]}_{char}_{NB_POINTS}_IoU.csv', 'w', newline='') as file:
                        writer = csv.writer(file)
                        writer.writerow(header_row)
                        # Write the results for varying frequencies and resolutions in csv file
                        for row in letter_frequencies_iou(font, char):
                            writer.writerow(row)
                    print(f"Font '{font_file[0]}', character {char} done")
            except Exception as e:
                print(e)
                print(f"Error handling font '{font_file[0]}', skipping")


if __name__ == '__main__':
    unittest.main()
